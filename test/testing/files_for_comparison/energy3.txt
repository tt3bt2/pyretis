# Cycle: 0, status: ACC
#     Time      Potential        Kinetic
         0      -0.964104
         1      -0.963482
         2      -0.962841
         3      -0.962185
         4      -0.961520
         5      -0.960848
# Cycle: 100, status: ACC
#     Time      Potential        Kinetic
         0      -0.964237
         1      -0.963746
         2      -0.963247
         3      -0.962731
         4      -0.962208
         5      -0.961693
# Cycle: 200, status: NCR
#     Time      Potential        Kinetic
         0      -0.964195
         1      -0.963582
         2      -0.962952
         3      -0.962322
         4      -0.961688
         5      -0.961051
