Extending PyRETIS with C
========================

This folder contains an example of extending PyRETIS with a new
integrator which is implemented in C.

The C code must be compiled before it can be executed and this
is done by running ``python setup.py build_ext --inplace``.
