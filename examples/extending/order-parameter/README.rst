Extending PyRETIS with a order parameter
========================================

This folder contains an example of how PyRETIS can be extended with a new
order parameter.

The order parameter is defined in the script ``orderparameter1.py`` and
which is used by the input script ``retis.rst``.

The example can be executed by:

pyretisrun -i retis.rst
