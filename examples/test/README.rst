########################
PyRETIS simulation tests
########################

This folder contains some simulation tests for PyRETIS which can be
used to verify the installation and that different parts of the
PyRETIS code work as intended. The different tests are described
below.

test-internal
-------------
This folder contains a set of tests for running PyRETIS with the
internal engine(s).

test-gromacs
------------
This folder contains a set of tests for running PyRETIS with
GROMACS as an external engine.

test-cp2k
---------
This folder contains a set of tests for running PyRETIS with
CP2K as an external engine.
