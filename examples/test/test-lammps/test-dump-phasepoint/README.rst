test-dump-phasepoint
====================

This test is indended to test that we can dump specific
phasepoints from a LAMMPS trajectory.

The test is executed using:

python test_lammps.py
