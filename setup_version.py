# -*- coding: utf-8 -*-
# Copyright (c) 2022, PyRETIS Development Team.
# Distributed under the LGPLv2.1+ License. See LICENSE for more info.
"""
PyRETIS - A simulation package for rare event simulations.

Copyright (c) 2022, PyRETIS Development team.

This file generates the version info and appends a ".beta"
to the logo if needed.

"""
import argparse
import json
import os
import pathlib
import subprocess


# Define path to files we are interacting with:
# 1) The file containing the current version info:
CURRENT_VERSION_FILE = pathlib.Path('version.json')
# 2) The file with version info accessible by the PyRETIS library:
VERSION_FILE = pathlib.Path('pyretis').joinpath('version.py')
# 3) The setup.py file:
SETUP_PY = pathlib.Path('setup.py')
# 4) The info.py file containing the logo:
INFO_FILE = pathlib.Path('pyretis').joinpath('info.py')

# Define formats for released and development versions:
VERSION_DEV_FMT = '{major:d}.{minor:d}.{micro:d}.dev{dev:d}'
VERSION_FMT = '{major:d}.{minor:d}.{micro:d}'

# Define the text for creating .py files for PyRETIS:
HEADER_TXT = """# -*- coding: utf-8 -*-
# Copyright (c) 2022, PyRETIS Development Team.
# Distributed under the LGPLv2.1+ License. See LICENSE for more info."""

VERSION_TXT = '''{header:s}
"""Version information for PyRETIS.

This file is generated by PyRETIS (``setup_version.py``)
"""
SHORT_VERSION = '{version:s}'
VERSION = '{version:s}'
FULL_VERSION = '{full_version:s}'
GIT_REVISION = '{git_revision:s}'
GIT_VERSION = '{git_version:s}'
RELEASE = {release:}

if not RELEASE:
    VERSION = GIT_VERSION
'''

INFO_TXT = '''{header:s}
"""This module just contains some info for PyRETIS.

Here we define the name of the program and some other
relevant info.

This file is generated by PyRETIS (``setup_version.py``)
"""
PROGRAM_NAME = 'PyRETIS'
URL = 'http://www.pyretis.org'
GIT_URL = 'https://gitlab.com/pyretis/pyretis'
CITE = """
[1] A. Lervik, E. Riccardi and T. S. van Erp, J. Comput. Chem., 2017
    doi: https://dx.doi.org/10.1002/jcc.24900
[2] E. Riccardi, A. Lervik, S. Roet, O. Aarøen and T. S. van Erp,
    J. Comput. Chem., 2019, doi: https://dx.doi.org/10.1002/jcc.26112
"""
LOGO = r"""{logo:s}"""
'''


# Define the PyRETIS logo:
LOGO = r"""
  _______                                     ___________
 /   ___ \         ___  ____  ____________   / __     __/
/_/\ \_/ /_  __   / _ \/ __/ /_  _/ / ___/  /_/ / // /
   /  __  / / /  / /_)  _/    / // /\ \        / // /
  / /   \ \/ /  / _  \ /__   / // /__\ \    __/ // /__
 /_/    _\  /  /_/ |_|___/  /_//_/_____/   /_________/
       /___/
"""


def generate_version_string(version):
    """Generate a string with the current PyRETIS version.

    Parameters
    ----------
    version : dict
        A dict containing the current PyRETIS version.

    Returns
    -------
    version_txt : string
        A string with the current PyRETIS version.

    """
    version_fmt = VERSION_FMT if version['release'] else VERSION_DEV_FMT
    return version_fmt.format(**version)


def get_git_version():
    """Obtain the git revision as a string.

    This method is adapted from Numpy's setup.py

    Returns
    -------
    git_revision : string
        The git revision, it the git revision could not be determined,
        a 'Unknown' will be returned.

    """
    git_revision = 'Unknown'
    try:
        env = {}
        for key in ('SYSTEMROOT', 'PATH'):
            val = os.environ.get(key)
            if val is not None:
                env[key] = val
        # LANGUAGE is used on win32
        env['LANGUAGE'] = 'C'
        env['LANG'] = 'C'
        env['LC_ALL'] = 'C'
        out = subprocess.Popen(['git', 'rev-parse', 'HEAD'],
                               stdout=subprocess.PIPE,
                               env=env).communicate()[0]
        git_revision = out.strip().decode('ascii')
    except OSError:
        git_revision = 'Unknown'
    return git_revision


def get_version_info(version):
    """Return the version number for PyRETIS.

    This method is adapted from Numpy's setup.py.

    Parameters
    ----------
    version : dict
        The current version information.

    Returns
    -------
    full_version : string
        The full version string for this release.
    git_revision : string
        The git revision number.

    """
    version_txt = generate_version_string(version)
    if pathlib.Path('.git').is_dir():
        git_revision = get_git_version()
    elif pathlib.Path(VERSION_FILE).is_file():
        try:
            from pyretis.version import git_revision
        except ImportError:
            raise ImportError(
                'Unable to import git_revision. Try removing '
                'pyretis/version.py and the build directory '
                'before building.'
            )
    else:
        git_revision = 'Unknown'
    if not version['release']:
        git_version = ''.join(
            [
                version_txt.split('dev')[0],
                'dev{:d}+'.format(version['dev']),
                git_revision[:7]
            ]
        )
    else:
        git_version = version_txt
    full_version = version_txt
    return full_version, git_revision, git_version


def write_version_py(version):
    """Create a file with the version info for PyRETIS.

    This method is adapted from Numpy's setup.py.

    Parameters
    ----------
    version : dict
        The dict containing the current version information.

    Returns
    -------
    full_version : string
        The current full version for PyRETIS

    """
    full_version, git_revision, git_version = get_version_info(version)
    version_txt = VERSION_TXT.format(
        header=HEADER_TXT,
        version=full_version,
        full_version=full_version,
        git_revision=git_revision,
        git_version=git_version,
        release=version['release'],
    )
    with open(VERSION_FILE, 'wt') as vfile:
        vfile.write(version_txt)
    return full_version


def write_version_in_setup_py(version):
    """Update version for setup.py."""
    tmp = []
    comment = '# Automatically set by setup_version.py'
    with open(SETUP_PY, 'r') as sfile:
        for lines in sfile:
            if lines.startswith('FULL_VERSION ='):
                tmp.append(
                    ("FULL_VERSION = '{}'  {}\n".format(version, comment))
                )
            else:
                tmp.append(lines)
    with open(SETUP_PY, 'wt') as sfile:
        for lines in tmp:
            sfile.write(lines)


def update_logo(released):
    """Update the logo according to this being a released version or not.

    If this is a released version, we remove the ".beta" from the logo
    (if present). Otherwise, we append a ".beta" to the logo (if a
    ".beta" is not already present).

    Parameters
    ----------
    released : boolean
        Determines if this is a released version or not.

    Returns
    -------
    logo_txt : string
        The current PyRETIS logo.

    """
    logo_txt = LOGO
    if released:
        if '.beta' in logo_txt:
            logo_txt = logo_txt.replace('.beta', '')
    else:
        if '.beta' not in logo_txt:
            # Note: This is specific for the current logo layout!
            split = logo_txt.split('\n')
            split[-3] = '{}.beta'.format(split[-3])
            logo_txt = '\n'.join(split)
    info_txt = INFO_TXT.format(
        header=HEADER_TXT,
        logo=logo_txt,
    )
    with open(INFO_FILE, 'w') as info_out:
        info_out.write(info_txt)


def bump_version(args, version):
    """Increment the version number if requested.

    Parameters
    ----------
    args : object like argparse.Namespace
        The arguments determining if we are to bump the version number.
    version : dict
        The current version.

    Returns
    -------
    new_version : dict
        The updated version (if an update is requested). Otherwise it
        is just a copy of the input version.

    """
    new_version = version.copy()
    if args.bump_dev:
        new_version['dev'] += 1
    if args.bump_micro:
        new_version['micro'] += 1
        new_version['dev'] = 0
    if args.bump_minor:
        new_version['minor'] += 1
        new_version['micro'] = 0
        new_version['dev'] = 0
    if args.bump_major:
        new_version['major'] += 1
        new_version['minor'] = 0
        new_version['micro'] = 0
        new_version['dev'] = 0
    return new_version


def main(args):
    """Generate version information and update the relevant files."""
    version = {}
    with open(CURRENT_VERSION_FILE, 'r') as json_file:
        version = json.load(json_file)
    version = bump_version(args, version)
    full_version = write_version_py(version)
    print('Setting version to: {}'.format(full_version))
    write_version_in_setup_py(full_version)
    update_logo(version['release'])
    with open(CURRENT_VERSION_FILE, 'w') as json_file:
        json.dump(version, json_file, indent=4)


def get_argument_parser():
    """Return a parser for arguments."""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--bump_major',
        action='store_true',
        help='Increment the major version.'
    )
    parser.add_argument(
        '--bump_minor',
        action='store_true',
        help='Increment the minor version.'
    )
    parser.add_argument(
        '--bump_micro',
        action='store_true',
        help='Increment the micro version.'
    )
    parser.add_argument(
        '--bump_dev',
        action='store_true',
        help='Increment the development version.'
    )
    return parser


if __name__ == '__main__':
    PARSER = get_argument_parser()
    main(PARSER.parse_args())
